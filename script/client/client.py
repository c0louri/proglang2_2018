#!/usr/bin/python

import sys
import requests
from bs4 import BeautifulSoup

# Python 3 implementation to find
# minimum number of deletions
# to make a string palindromic
# Source : GeekforGeeks
def lps(str):
    n = len(str)
    L = [[0 for x in range(n)]for y in range(n)]
    for i in range(n):
        L[i][i] = 1
    for cl in range(2, n+1):
        for i in range(n - cl + 1):
            j = i + cl - 1
            if (str[i] == str[j] and cl == 2):
                L[i][j] = 2
            elif (str[i] == str[j]):
                L[i][j] = L[i + 1][j - 1] + 2
            else:
                L[i][j] = max(L[i][j - 1], L[i + 1][j])
    return L[0][n - 1]


def minimumNumberOfDeletions(str):
    n = len(str)
    len_pal = lps(str)
    return (n - len_pal)

# η solve() ειναι υπευθυνη για την λυση των προβληματων
# Δεχεται την ιστοσελιδα στην οποια υπαρχει το παιχνιδι και 
# τερματιζει οταν λυσει ολες τις ερωτησεις
# Για καθε ερωτηση γινονται τα εξης:
#   Με το Requests ζητειται το περιεχομενο της σελιδας
#   Στην συνεχεια με το BeautifulSoup προσπελασεται το http content
#   της απαντησης του server, ώστε να βρεθει το string της ερωτησης
#   Επειτα καλειται η minimumNumberOfDeletions() που επιστεφει την απαντηση,
#   η οποια στελνεται στον server μεσω post request
#   Τελος, ελεγχεται με την βοηθεια του BeautifulSoup,
#   αν η απαντηση ηταν σωστη και αν απαντηθηκαν ολες οι ερωτησεις

def solve(url):
    round = 0
    s = requests.Session()
    while True:
        round = round + 1
        page = s.get(url)
        soup = BeautifulSoup(page.content, features="lxml")
        q = soup.find(id="question")
        if q is None:
            print("Error finding Question!")
            quit()
        str = q.text
        print("Round %d, length %d, %s" % (round, len(str), str))
        sol = minimumNumberOfDeletions(str)
        print("Answer: %d" % (sol))
        data = {"answer": sol, "submit": ""}
        page = s.post(url, data=data)  # html requests with the solution
        # Check if the solution was the right one
        soup = BeautifulSoup(page.content, features="lxml")
        a = soup.find(class_="right")
        if a is None:
            # Wrong Answer or Problem
            a = soup.find(class_="wrong")
            if a is None:
                print("Error checking answer!")
                quit()
        # Print if answer is right or wrong
        print(a.text)
        # Check if program has answered all questions
        congrat = soup.find(class_="congratulations")
        if congrat is not None:
            print(congrat.text)
            quit()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("No URL has been given")
        quit()
    solve(sys.argv[1])
