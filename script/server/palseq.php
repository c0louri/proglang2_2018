<?php
// PHP implementation to find
// minimum number of deletions
// to make a string palindromic
// Source : GeekforGeeks
    function lps($str)
    {
        $n = strlen($str);
        $L;
        for ($i = 0; $i < $n; $i++)
            $L[$i][$i] = 1;
        for ($cl = 2; $cl <= $n; $cl++) {
            for ( $i = 0; $i < $n -$cl + 1;  $i++) {
                $j = $i + $cl - 1;
                if ($str[$i] == $str[$j] && $cl == 2)
                    $L[$i][$j] = 2;
                else if ($str[$i] == $str[$j])
                    $L[$i][$j] = $L[$i + 1][$j - 1] + 2;
                else
                    $L[$i][$j] = max($L[$i][$j - 1], $L[$i + 1][$j]);
            }
        }
        return $L[0][$n - 1];
    }

    function minimumNumberOfDeletions($str) {
        $n = strlen($str);
        $len = lps($str);
        return ($n - $len);
    }

    function new_pal($round){
        $length = rand(50*($round-1) + 1, 50*$round);
        # $length = 5;
        # $str = "abbab";
        # $sol = 1;
        $str = generate_str($length, $round);
        $sol = minimumNumberOfDeletions($str);
        return array($str, $sol, $length);
    }

    function generate_str($length, $round){
        $chars = 'abcdefghijklmnopqrstuvwxyz';
        $chars_len = strlen($chars);
        $random_str = '';
        for($i = 0; $i < $length; $i++) {
            $random_str .= $chars[rand(0, 2*$round)];
        }
        return $random_str;
    }

?>

<?php
// Για να κρατάμε τιμές απο round σε round, γινεται του χρήση του session του
// php, μέσα στο οποίο κρατούνται οι τιμές για τον αριθμό του γύρου, κλπ
// Έτσι επιτυγχάνεται το ότι ένας client θα αντιμετωπίσει πεπερασμένο αριθμό 
// ερωτήσεων (10) κλιμακούμενης δυσκολίας
// Κάθε session έχει τα εξής πεδία:
// next -> flag για το αν θα δωθεί νέα ερώτηση (αλλάζει κάθε φορά που ένας 
//  client απαντα σωστά σε True και αντίστοιχα σε False όταν πρωτοδημιουργείται
//  η επόμενη ερώτηση)
// string, solution, length -> είναι αποθηκευμένες οι αντιστοιχες τιμές που χρειάζονται
//  σε κάθε ερώτηση. Αυτές ανανεώνονται κάθε φορά που χρειάζεται μία νέα ερώτηση, έτσι
//  σε περίπτωση λάθους απάντησης, δίνεται η ίδια ερώτηση
    session_start();
    # initialize variables for round
    if(!isset($_SESSION['round']))
        $_SESSION['round'] = 0;
    if(!isset($_SESSION['next']))
        $_SESSION['next'] = TRUE;

    $page = htmlentities($_SERVER['PHP_SELF']);
    $next = $_SESSION['next'];
    if($next){
        $_SESSION['round']++;
        //crete question, etc for the next round
        list($_SESSION['string'], $_SESSION['solution'], $_SESSION['length'])
            = new_pal($_SESSION['round']);
        $_SESSION['next'] = FALSE;
    }
    $round = $_SESSION['round'];
    $length = $_SESSION['length'];
    $str = $_SESSION['string'];
    $sol = $_SESSION['solution'];
?>


<!DOCTYPE html PUBLIC
          "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Find the longest palindrome!</title>
<style type="text/css">
<!--
body,td,th {
  font-family: Verdana, Arial, Helvetica, sans-serif;
  font-size: x-large;
  color: #CCCCCC;
}

body {
  background-color: #333399;
}

.question { color: #FFCC33; }
.emph     { color: #99ee99; }
.alert    { color: #ee77aa; }

.right {
  color: #33FF66;
  font-weight: bold;
}
.wrong {
  color: #FF3366;
  font-weight: bold;
}

a:link    { color: #CCFFFF; }
a:visited { color: #CCFFFF; }

input {
  background-color: #eeee66;
  color: #333399;
}

code {
  font-family: Consolas, "Andale Mono", "Courier New", monospace, sans-serif;
  color: #eeee99;
  font-size: 120%;
}

span.removed {
  color: #ff9977;
  text-decoration: underline red;
}

code.block {
  background-color: #66eeee;
  color: #993333;
  overflow-wrap: break-word;
  display: block;
  border: 1px solid black;
  padding: 8px;
  width: 95%;
  line-height: 1em;
  margin-top: 0.25em;
  margin-bottom: 0.25em;
}

input.box {
  overflow-wrap: break-word;
  font-family: Consolas, "Andale Mono", "Courier New", monospace, sans-serif;
  font-size: 120%;
  color: #333333;
  border: 1px solid black;
  padding: 8px;
}

input.button {
  font-size: 120%;
  background-color: #99ee99;
  color: #333399;
  border: 1px solid black;
  padding: 8px;
}

-->
</style>
</head>
<body>
<h1>Find the longest palindrome!</h1>

<p>I'll give you a string of (up to 1000) letters
  and I need you to do one simple thing:
</p>
<p>Find the <span class="emph">least</span> possible number of letters that,
  if removed from the given string, what remains is a
  <span class="emph">palindrome</span>.
</p>
<blockquote>
  <p>For example, given the string:
    <code>bbccaddabaddacaaacdb</code>
    the correct answer is <span class="emph">5</span>.
  </p>
  <p>If one removes these five underlined letters:
    <code>b<span class="removed">b</span>ccaddabaddWrong!  Try again...  :-(ac<span class="removed">aaa</span>c<span class="removed">d</span>b</code>
    then the remaining string:
    <code>bccaddabaddaccb</code>
    is indeed a palindrome.  It is not possible to obtain a
    palindrome by removing fewer than five letters.
  </p>
</blockquote>

<hr />

<?php
    # check if an answer has been submitted
    if(isset($_POST['submit'])){
        # Print the question for which the answer was submitted
        echo '<p><span class="question">Question ' . $round . '</span>:
        length ' . $length . '  <code class="block" id="question">' . $str . '</code>
        </p>';
        $answer = $_POST['answer'];
        if($answer == $sol){
            # the answer is right, proceed to the next round
            $_SESSION['next'] = TRUE;
            echo '<p class="right">Right!  :-)</p>';
            # check if it was the last round
            if ($_SESSION['round'] == 10) {
                # the right answer was for the last question (round 10)
                echo '<p class="congratulations">Congratulations!!!</p>';
                # Το παιχνίδι ολοκληρώθηκε με τον παίκτη, οπότε καταστρέφεται το session
                # έτσι ο ίδιος client μπορέι να το παίξει ξανά από την αρχή
                session_unset();
                session_destroy();
            }
        }
        else {
            # the asnwer is wrong, repeat the question
            echo '<p class="wrong">Wrong!  Try again...  :-(</p>';
        }
        echo '<br>
        <form action="'. $page .'" id="r" name="r" method="post">
            <input class="button" type="submit" name="again" id="again" value="Continue!" autofocus />
        </form>';
    }
    else {
        # show question
        echo '<p><span class="question">Question ' . $round . '</span>:
        length ' . $length . '  <code class="block" id="question">' . $str . '</code>
        </p>';

        echo '<form action="' . $page . '" id="f" name="f" method="post">
        What is the least number of characters you need to remove?
        <table border="0" cellspacing="3">
        <tr>
          <td><input type="text" class="box" name="answer" id="answer" autofocus /></td>
          <td><input type="submit" class="button" name="submit" id="submit" value="Submit!" /></td>
        </tr>
        </table>
        </form>';
    }
?>

</body>
</html>
