data Tree a = T a [Tree a]
    deriving Show

foldTree :: (a -> [b] -> b) -> Tree a -> b
foldTree f (T a t) = f a (map (foldTree f) t)

sizeTree :: Num b => Tree a -> b
sizeTree = foldTree (\_ a -> if null a then 1 else 1 + sum a)

heightTree :: (Ord b, Num b) => Tree a -> b
heightTree = foldTree (\_ a -> if null a then 1 else 1 + maximum a)

sumTree :: Num a => Tree a -> a
sumTree = foldTree (\x a -> x + sum a)

maxTree :: Ord a => Tree a -> a
maxTree = foldTree aux where
    aux x [] = x
    aux x a = if b > x then b else x where b = maximum a

inTree :: Eq a => a -> Tree a -> Bool
inTree x = foldTree (\a t -> (x == a) || (True `elem` t))

nodes :: Tree a -> [a]
nodes = foldTree (\a l -> concat ([a] : l))

countTree :: (a -> Bool) -> Tree a -> Integer
countTree f = foldTree (\a l -> sum l + if f a then 1 else 0)

leaves :: Tree a -> [a]
leaves = foldTree aux where
    aux x [] = [x]
    aux _ l  = concat l

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f = foldTree (\x l -> T (f x) l)

trimTree :: Int -> Tree a -> Tree a
trimTree n = aux 0
    where
        aux i (T x ts) = T x ts'
          where ts' = if i + 1 < n then auxs (i+1) ts else []
        auxs _ [] = []
        auxs i' ts = map (aux i') ts


path :: [Int] -> Tree a -> a
path [] (T a _) = a
path (x:xs) (T _ ts) = path xs (head(drop x ts))
