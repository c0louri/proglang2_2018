data Tree a = T a [Tree a]
    deriving Show

-- examples of trees
t1 = T 1 [ T 2 [ T 3 []
                     , T 4 []
                     ]
            , T 5 [ T 6 [] ]
            ]

t2 = T 'a' [ T 'b' []
              , T 'c' [ T 'e' []
                         , T 'f' []
                         ]
              , T 'd' []
              ]
