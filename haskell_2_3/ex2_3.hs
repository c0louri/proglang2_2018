import Test.QuickCheck
import Test.QuickCheck.Function
import GHC.Real

--
-- 1st part of the file is for the 2nd exercise (functions for datatype Tree)
--

data Tree a = T a [Tree a]
    deriving Show

foldTree :: (a -> [b] -> b) -> Tree a -> b
foldTree f (T a t) = f a (map (foldTree f) t)

sizeTree :: Num b => Tree a -> b
sizeTree = foldTree (\_ a -> if null a then 1 else 1 + sum a)

heightTree :: (Ord b, Num b) => Tree a -> b
heightTree = foldTree (\_ a -> if null a then 1 else 1 + maximum a)

sumTree :: Num a => Tree a -> a
sumTree = foldTree (\x a -> x + sum a)

maxTree :: Ord a => Tree a -> a
maxTree = foldTree aux where
    aux x [] = x
    aux x a = if b > x then b else x where b = maximum a

inTree :: Eq a => a -> Tree a -> Bool
inTree x = foldTree (\a t -> (x == a) || (True `elem` t))

nodes :: Tree a -> [a]
nodes = foldTree (\a l -> concat ([a] : l))

countTree :: (a -> Bool) -> Tree a -> Integer
countTree f = foldTree (\a l -> sum l + if f a then 1 else 0)

leaves :: Tree a -> [a]
leaves = foldTree aux where
    aux x [] = [x]
    aux _ l  = concat l

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f = foldTree (\x l -> T (f x) l)

trimTree :: Int -> Tree a -> Tree a
trimTree n = aux 0
    where
        aux i (T x ts) = T x ts'
          where ts' = if i + 1 < n then auxs (i+1) ts else []
        auxs _ [] = []
        auxs i' ts = map (aux i') ts


path :: [Int] -> Tree a -> a
path [] (T a _) = a
path (x:xs) (T _ ts) = path xs (head(drop x ts))


--
-- 2nd part is for QuickCheck exercise
--

-- Generator function for datatype Tree

instance Arbitrary a => Arbitrary (Tree a) where
    arbitrary = do
        n <- getSize
        tree n

tree :: Arbitrary a => Int -> Gen (Tree a)
tree n = do
    ch <- choose (0,n `mod` 4)      --
    val <- arbitrary                -- value of the node
    chs <- vectorOf ch (tree (n `div` 2))
    return (T val chs)


--Functions for checking specific properties of datatype Tree

--1) check if heightTree > 0 and <= sizeTree
prop_height :: Tree a -> Bool
prop_height t = heightTree t > 0 && heightTree t <= sizeTree t

--2) check if maxTree  belongs to the Tree
prop_max :: (Eq a,Ord a) => Tree a -> Bool
prop_max t = inTree (maxTree t) t

--3) check if all nodes from (nodes t) belong to the tree t
prop_nodes :: Eq a => Tree a -> Bool
prop_nodes t = aux t (nodes t)
    where
        aux _ [] = True
        aux t (n:ls) = inTree n t && aux t ls

--4) check if countTree result is valid
prop_countTree :: Fun a Bool-> Tree a -> Bool
prop_countTree f t = a >= 0 && a <= sizeTree t
    where
        a = countTree (apply f) t

--5) check sizeTree = #nodes and #leafs < sizeTree
prop_nodes_leaves :: Tree a -> Bool
prop_nodes_leaves t = (s == n) && if s == 1 then l == s else l < s
    where
        s = sizeTree t
        n = length (nodes t)
        l = length (leaves t)

--6) check if mapTree result is valid
prop_mapTree :: Fun a b -> Tree a -> Bool
prop_mapTree f t = (s1 == s2) && (h1 == h2)
    where
        (s1, h1) = (sizeTree t, heightTree t)
        (s2, h2) = (sizeTree t', heightTree t') where t' = mapTree (apply f) t

--7) check if n belong to t then, f n belongs to mapTree f t
prop_n_fn_mapTree :: (Eq a, Eq b) => Fun a b -> Tree a -> a -> Bool
prop_n_fn_mapTree f t n = not (inTree n t) || inTree (apply f n) (mapTree (apply f) t)

--8) check map,mapTree with nodes,leaves
prop_map_mapTree :: Eq b => Fun a b -> Tree a -> Bool
prop_map_mapTree f t = map (apply f) (nodes t) == nodes (mapTree (apply f) t) &&
                        map (apply f) (leaves t) == leaves (mapTree (apply f) t)


-- Definition of Bird tree

bird :: Tree Rational
bird = T 1 [mapTree (f . g) bird, mapTree (g . f) bird]
    where
        f :: Rational -> Rational
        f x = 1 / x
        g :: Rational -> Rational
        g x = x + 1


---Functions for checking specific properties for Bird


--1)Check path at Bird and trimTree n bird

newtype Path = P [Int] deriving (Eq,Show)
instance Arbitrary Path where
    arbitrary = do
        n <- getSize
        ls <- vectorOf n (elements [0,1])
        return (P ls)

prop_path_bird :: Path -> Bool
prop_path_bird (P l) = path l bird == path l (trimTree (1 + length l) bird)

--2) Check if right-left path gets through all Naturals

prop_path_natural :: Positive Int -> Bool
prop_path_natural n = aux 'r' 1 bird
    where
        aux  _  n (T v _ ) =  v == n
        aux 'r' i (T v (a:_)) = aux 'l' (i+1) a
        aux 'l' i (T v (a:b:_)) = aux 'r' (i+1) b

--3) Check far-left path and fibonacci sequence

prop_path_fib ::  Positive Int -> Bool
prop_path_fib n = aux 2 bird
    where
        aux n (T v _) = denominator v == fib n
        aux i (T v (a:_)) = (denominator v == fib i) && aux (i+1) a
        fib 0 = 0
        fib 1 = 1
        fib n = fib (n-1) + fib (n-2)

--4) Check if every Positive Rational exists somewhere in the Bird tree

findBird :: Rational -> [Char]
findBird q = aux bird 0
    where
        aux (T t (a:b:_)) 0 = if q == t then [] else
                if q < t then 'l' : aux a 1 else 'r' : aux b 1
        aux (T t (a:b:_)) 1 = if q == t then [] else
                if q < t then 'r' : aux b 0 else 'l' : aux a 0

prop_every_rational :: Positive Rational -> Bool
prop_every_rational (Positive q) = aux ls bird
    where
        ls = findBird q
        aux :: [Char] -> Tree Rational -> Bool
        aux [] (T t _) = q == t
        aux ('l':xs) (T t (a:b:_)) = aux xs a
        aux ('r':xs) (T t (a:b:_)) = aux xs b

---
---Functions fot testing prop_* functions
---

checkPropertiesTree = do
    putStrLn "Checking if heightTree > 0 and <= sizeTree"
    quickCheck (prop_height :: Tree Int -> Bool)
    putStrLn "Checking if maxTree  belongs to the Tree"
    quickCheck (prop_max :: Tree Int -> Bool)
    putStrLn "Checking if all nodes in (nodes t) belong to the tree t"
    quickCheck (prop_nodes :: Tree Int -> Bool)
    putStrLn "Checking if countTree result is valid"
    quickCheck (prop_countTree :: Fun Int Bool-> Tree Int -> Bool)
    putStrLn "Checking sizeTree = #nodes and #leafs < sizeTree"
    quickCheck (prop_nodes_leaves :: Tree Int -> Bool)
    putStrLn "Checking if mapTree result is valid"
    quickCheck (prop_mapTree :: Fun Int Int -> Tree Int -> Bool)
    putStrLn "Checking if n belongs to t then f n belongs to mapTree f t"
    quickCheck (prop_n_fn_mapTree :: Fun Int Int -> Tree Int -> Int -> Bool)
    putStrLn "Checking nodes,leaves functions with map and mapTree"
    quickCheck (prop_map_mapTree :: Fun Int Int -> Tree Int -> Bool)


checkPropertiesBird = do
    putStrLn "Check path at Bird and trimTree n bird"
    quickCheck prop_path_bird
    putStrLn "Check if right-left path gets through all Naturals"
    quickCheck prop_path_natural
    putStrLn "Check far-left path and fibonacci sequence"
    quickCheck prop_path_fib
    putStrLn "Check if every Positive Rational exists somewhere in the Bird tree"
    quickCheck prop_every_rational


main = do
    putStrLn "!Run tests for Tree!"
    checkPropertiesTree
    putStrLn "\n!Run tests for Bird!"
    checkPropertiesBird
