#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdint>
#include <vector>
#include <ctime>
using namespace std;

#define HALT 0x00
#define JUMP 0x01
#define JNZ 0x02
#define DUP 0x03
#define SWAP 0x04
#define DROP 0x05
#define PUSH4 0x06
#define PUSH2 0x07
#define PUSH1 0x08
#define ADD 0x09
#define SUB 0x0a
#define MUL 0x0b
#define DIV 0x0c
#define MOD 0x0d
#define EQ 0x0e
#define NE 0x0f
#define LT 0x10
#define GT 0x11
#define LE 0x12
#define GE 0x13
#define NOT 0x14
#define AND 0x15
#define OR 0x16
#define INPUT 0x17
#define OUTPUT 0x18
#define CLOCK 0x2a
#define CONS 0x30
#define HD 0x31
#define TL 0x32

#define NEXT_INSTRUCTION goto *label_tab[*pc]

int32_t a32, b32;
uint32_t a_tag, b_tag;
uint8_t *pc;            // pointer for Program Counter
uint8_t io_ch;          // used for io operations
uint8_t opcode;
uint8_t code[65537] = {0};
vector<uint32_t> stack;

//*****************
// For GC:
// Garbage collector implements MARK N SWEEP
// GC is executed only if heap is full ( when available cons cells== 0)
// Also can increase the allocated memory for heap, which can be increased by 2^15
// cons cell each time this is needed

// Cons cell:
struct cons_cell_struct {
    uint32_t head;
    uint32_t tail;
    uint8_t tag;
} cons_cell;

#define HEAP_SIZE  32768
// Index for the heap, specifically for tagging
// if == 1 is a live cons cell and if it is marked is == 2
// vector<uint8_t> heap_index(HEAP_SIZE, 0);
// HEAP_SIZE : total number of heap-allocated cons cells
vector<cons_cell_struct> heap(HEAP_SIZE); // 2^15 cons cells
// Vector with indexes to cons cells who have been freed previously by garbage collector:
vector<uint32_t> freed_cells;
// at the start, available heap's size is HEAP_SIZE
// If it is full and there are no garbages then
// create a new
uint32_t available = HEAP_SIZE;
uint32_t next_available_cell = 0; // initialized at the begining of the heap
// used for fast allocating of a new cons cell
// By using next_available_cell and the vector of freed cells ,almost constant time
// is achieved for the insertion of a cons cell, if there is available space

void increase_size_heap() {
    // this function is called only if heap is full with non-garbage cons cells
    printf("Increase heap size...\n");
    uint32_t old_size = heap.size();
    try {
        heap.resize(heap.size() + HEAP_SIZE);
        //heap_index.resize(heap_index.size() + HEAP_SIZE, 0);
    }
    catch (bad_alloc const&) {
        printf("Memory allocation fail!\n");
    }
    available += HEAP_SIZE;
    next_available_cell = old_size;
}


inline int get_cons_cell(uint32_t address) {
    if (address >= 0 && address < heap.size()) {
        // valid address
        // Retrieve cons cell value
        cons_cell = heap[address];
        return 0;
    }
    else {
        // Address is invalid
        return -1;
    }
}

inline void get_signed_a() {
    a_tag = stack.back() & 0x00000001;
    if(stack.back() & 0x80000000)  {
        // > 0 -> needs sign extension
        a32 = (stack.back() >> 1) | 0x80000000;
    } else {
        // no need for sign extension
        a32 = stack.back() >> 1;
    }
    stack.pop_back();
}

inline void get_signed_b() {
    b_tag = stack.back() & 0x01;
    if(stack.back() & 0x80000000)  {
        // > 0 -> needs sign extension
        b32 = (stack.back() >> 1) | 0x80000000;
    } else {
        // no need for sign extension
        b32 = stack.back() >> 1;
    }
    stack.pop_back();
}

inline void push_word(uint32_t val) {
    // word is NOT an address, lsb (tag bit) = 0b0
    stack.push_back((val << 1));
}

inline void push_addr(uint32_t val) {
    // word is address, lsb (tag bit) = 0b1
    stack.push_back((val << 1) | 0x00000001);
}


// Function for marking garbage cons cells
void mark(uint32_t addr) {
    // Check if cons cell contains address
    if (heap[addr].head & 0x01) {
        uint32_t new_addr = heap[addr].head >> 1;
        if(heap[new_addr].tag == 0x00000001) {
            heap[new_addr].tag = 0x00000002;
            mark(new_addr);
        }
    }
    if (heap[addr].tail & 0x01) {
        uint32_t new_addr = heap[addr].tail >> 1;
        if(heap[new_addr].tag == 0x00000001) {
            heap[new_addr].tag = 0x00000002;
            mark(new_addr);
        }
    }

}

// Function for finding root addresses in stack
void init_mark() {
    // Search stack elements for addresses
    for(auto el = stack.begin(); el != stack.end(); el++) {
        // Check address tag
        if (*el & 0x01) {
            // is address and mark i
            if(heap[*el >> 1].tag == 0x00000001) {
                heap[*el >> 1].tag = 0x00000002;
                mark(*el >> 1);
            }
        }
    }
}

// Function for deleting garbage and unmarking cons cells
void sweep() {
    for(uint32_t i = 0; i < next_available_cell; i++) {
        if (heap[i].tag == 0x01) {
            // i cons cell is not marked, so must be freed
            // mark cons cell as unused
            heap[i].tag = 0x00;
            // add addr to the ones which can be used again
            freed_cells.push_back(i);
            available++;
        } else if (heap[i].tag == 0x02) {
            // restore tags for non-garbage cons cells
            heap[i].tag = 0x01;
        }
    }
}

// Function for allocating memory and executing garbage collector
int8_t add_cons_cell_to_heap(){
    // if memory full run garbage collector : MARK AND SWEEP
    if ( available == 0) {
        // First mark garbage
        init_mark();
        // Then delete marked garbage and unmark cons cells
        sweep();
    }
    // Check again if there is available memory because
    // Garbage collector might not have found any garbage cons cells
    if (available == 0) {
        // heap is full and no garbage exists, so increase heap size
        increase_size_heap();
    }
    // Find free memory
    // Firstly ,try ose one of the previously freed cells
    uint32_t addr = -1;
    while(!freed_cells.empty()) {
        addr = freed_cells.back();
        freed_cells.pop_back();
        if(addr < heap.size() && heap[addr].tag == 0x00) {
            heap[addr] = cons_cell;
            // heap_index[addr] = 0x01;
            push_addr(addr);
            available--;
            return 0;
        }
    }
    heap[next_available_cell] = cons_cell;
    // heap_index[next_available_cell] = 0x01;
    // Push addr of cons cell to stack
    push_addr(next_available_cell++);
    available--;
    return 0;
}


int main(int argc, char const *argv[]) {
    if (argc != 2) {
        printf("Wrong number of arguments!\n");
        exit(1);
    }
    // Read from file, name given in argv
    FILE *bytecode;
    bytecode = fopen(argv[1], "rb");
    uint16_t bytes_read = 0;
    if ((bytes_read = fread(code, 1, 65536, bytecode)) <= 0) {
        printf("Problem with reading the file...\n");
        fclose(bytecode);
        exit(1);
    }
    fclose(bytecode);
    // Initializing pointers to the labels for Idirect Threaded Interpreter
    static void *label_tab[ 0x33 ] = {
        &&halt_lb, &&jump_lb, &&jnz_lb, &&dup_lb,
        &&swap_lb ,&&drop_lb ,&&push4_lb, &&push2_lb, &&push1_lb,
        &&add_lb, &&sub_lb, &&mul_lb, &&div_lb, &&mod_lb,
        &&eq_lb, &&ne_lb, &&lt_lb, &&gt_lb, &&le_lb, &&ge_lb,
        &&not_lb, &&and_lb, &&or_lb, &&input_lb, &&output_lb, &&error_lb
    };
    label_tab[ 0x2a ] = &&clock_lb;
    // Labels for version with Garbage Collection:
    label_tab[ 0x30 ] = &&cons_lb;
    label_tab[ 0x31 ] = &&hd_lb;
    label_tab[ 0x32 ] = &&tl_lb;
    // Last byte of code[] (size = 65536+1) is always 0, for programs with no
    // HALT at the end to finish
    // For Indirect Threaded Interpeter , execute following goto (bypass switch)
    // Below exists code for switch-based and indirect threaded Interpreter
    // The code for switch-based is commented out
    // By default it is indirect threaded(switch-break etc are not executed)
    // Easily, we can change type by commenting goto commands and uncommenting
    // while, opcode, switch-case, breaks
    // Code below is for a little-endian machine

    // Save starting time
    clock_t start_time = clock();
    // Point PC to the 1st command
    pc = &code[0];
    goto *label_tab[*pc];
    while(true) {
        opcode = *pc;
        // printf("OPCODE = %x\n",opcode);
        switch(opcode) {
            // HALT - TERMINATE PROGRAM
            case HALT:
            halt_lb:
                return 0;
                break;
            // JUMP TO ADDRESS
            case JUMP:
            jump_lb:
                pc++;
                pc = &code[0] + *((uint16_t*)pc);
                NEXT_INSTRUCTION;
                break;
            // JUMP IF NOT ZERO
            case JNZ:
            jnz_lb:;
                pc++;
                if(stack.back() != 0) {
                    pc = &code[0] + *((uint16_t*)pc); // jump
                }
                else pc += 2; // dont jump
                stack.pop_back();
                NEXT_INSTRUCTION;
                break;
            // ADD DUPLICATE
            case DUP:
            dup_lb:
                pc++;
                // printf("DUP with %d\n", *pc);
                stack.push_back(stack[(stack.size()-1) - *pc]);
                // cout << "New stack top is : " << bitset<32>(stack.back()) << endl;
                pc++;
                NEXT_INSTRUCTION;
                break;
            // SWAP STACK ELEMENTS
            case SWAP:
            swap_lb:
                if(stack.size() <= *(pc++)) {
                    printf("Not enough elements for SWAP.\nTerminating...\n");
                    exit(1);
                }
                // printf("SWAP with %d", *pc);
                swap(stack[stack.size()-1], stack[(stack.size()-1) - *pc]);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // DROP TOP ELEMENT IN STACK
            case DROP:
            drop_lb:
                if (stack.size() == 0) {
                    printf("No signed();element to DROP.\nTerminating...\n");
                    exit(1);
                }
                stack.pop_back();
                pc++;
                NEXT_INSTRUCTION;
                break;
            // PUSH 4 BYTES INTO STACK
            case PUSH4:
            push4_lb:
                pc++;
                push_word(*(int32_t*)pc);
                pc += 4;
                NEXT_INSTRUCTION;
                break;
            case PUSH2:
            push2_lb:
                pc++;
                push_word(*(int16_t*)pc);
                pc += 2;
                NEXT_INSTRUCTION;
                break;
            case PUSH1:
            push1_lb:
                pc++;
                push_word(*(int8_t*)pc);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // ADD
            case ADD:
            add_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for AND\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                // Compine result and tag and push int stack
                stack.push_back(((a32 + b32) << 1) | (a_tag | b_tag));
                pc++;
                NEXT_INSTRUCTION;
                break;
            // SUB
            case SUB:
            sub_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for SUB\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                // Compine result and tag and push int stack
                stack.push_back(((a32 - b32) << 1) | (a_tag | b_tag));
                pc++;
                NEXT_INSTRUCTION;
                break;
            // MUL
            case MUL:
            mul_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for MUL\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                // Compine result and tag and push int stack
                stack.push_back(((a32 * b32) << 1) | (a_tag | b_tag));
                pc++;
                NEXT_INSTRUCTION;
                break;
            // DIV
            case DIV:
            div_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for DIV\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                // Compine result and tag and push int stack
                stack.push_back(((a32 / b32) << 1) | (a_tag | b_tag));
                pc++;
                NEXT_INSTRUCTION;
                break;
            // MOD
            case MOD:
            mod_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for MOD\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                // Compine result and tag and push int stack
                stack.push_back(((a32 % b32) << 1) | (a_tag | b_tag));
                pc++;
                NEXT_INSTRUCTION;
                break;
            // EQUAL
            case EQ:
            eq_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for EQ\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                if(a32 == b32) stack.push_back(1 << 1);
                else stack.push_back(0 << 1);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // NOT EQUAL
            case NE:
            ne_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for NE\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                if(a32 != b32) stack.push_back(1 << 1);
                else stack.push_back(0 << 1);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // LESS
            case LT:
            lt_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for LT \n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                if(a32 < b32) stack.push_back(1 << 1);
                else stack.push_back(0 << 1);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // GREATER
            case GT:
            gt_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for GT\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                if(a32 > b32) stack.push_back(1 << 1);
                else stack.push_back(0 << 1);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // LESS OR EQUAL
            case LE:
            le_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for LE\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                if(a32 <= b32) stack.push_back(1 << 1);
                else stack.push_back(0 << 1);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // GREATER OR EQUAL
            case GE:
            ge_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for GE\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                if(a32 >= b32) stack.push_back(1 << 1);
                else stack.push_back(0 << 1);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // NOT
            case NOT:
            not_lb:
                if (stack.size() < 1) {
                    printf("Not enough elements in Stack for NOT\n");
                    exit(1);
                }
                get_signed_a();
                if(a32 == 0) stack.push_back(1 << 1);
                else stack.push_back(0 << 1);
                pc++;
                NEXT_INSTRUCTION;
                break;
            //AND
            case AND:
            and_lb:
                if (stack.size() < 1) {
                    printf("Not enough elements in Stack for AND\n");
                    exit(1);
                }
                get_signed_b();
                get_signed_a();
                if(a32 && b32) stack.push_back(1 << 1);
                else stack.push_back(0 << 0);
                pc++;
                NEXT_INSTRUCTION;
                break;
            case OR:
            or_lb:
                if (stack.size() < 1) {
                    printf("Not enough elements in Stack for OR\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                if(a32 || b32) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // INPUT
            case INPUT:
            input_lb:
                scanf("%c", &io_ch);
                push_word(io_ch);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // OUTPUT
            case OUTPUT:
            output_lb:
                if (stack.size() < 1) {
                    printf("Empty stack. No available output.\n");
                    exit(1);
                }
                get_signed_a();
                io_ch = a32;
                printf("%c", io_ch);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // CLOCK
            case CLOCK:
            clock_lb:
                printf("%0.6lf\n", (double)(clock() - start_time) / CLOCKS_PER_SEC);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // Create new cons cell
            case CONS:
            cons_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for creating CONS CELL\n");
                    exit(1);
                }
                cons_cell.tail = stack.back();
                stack.pop_back();
                cons_cell.head = stack.back();
                stack.pop_back();
                cons_cell.tag = 0x01;
                // Add cons_cell to heap
                if (add_cons_cell_to_heap() < 0) {
                    printf("Allocation of memory for cons cell failed...\n");
                    exit(1);
                }
                pc++;
                NEXT_INSTRUCTION;
                break;
            // Push to stach HEAD element of a cons cell
            case HD:
            hd_lb:
                // Get from a cons cell its tail
                get_signed_a();
                // Check if stack element is address
                if (a_tag == 0) {
                    printf("Trying accesing heap with a non-adress element...Address : %d\n", a32);
                    exit(1);
                }
                if (get_cons_cell(a32) < 0) {
                    printf("No available cons cell with the given adress.\n");
                    exit(1);
                }
                stack.push_back(cons_cell.head);
                pc++;
                NEXT_INSTRUCTION;
                break;
            // Push to stack TAIL element of a cons cell
            case TL:
            tl_lb:
                // Get from a cons cell its tail
                get_signed_a();
                // Check if stack element is address
                if (a_tag == 0) {
                    printf("Trying accesing heap with a non-adress element...Address : %d\n", a32);
                    exit(1);
                }
                if (get_cons_cell(a32) < 0) {
                    printf("No available cons cell with the given adress.\n");
                    exit(1);
                }
                stack.push_back(cons_cell.tail);
                pc++;
                NEXT_INSTRUCTION;
                break;
            default:
            error_lb:
                printf("ERROR WITH OPCODE...\n");
                exit(1);
        }
    }
    return 0;
}
