#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdint>
#include <vector>
#include <ctime>
using namespace std;

#define HALT 0x00
#define JUMP 0x01
#define JNZ 0x02
#define DUP 0x03
#define SWAP 0x04
#define DROP 0x05
#define PUSH4 0x06
#define PUSH2 0x07
#define PUSH1 0x08
#define ADD 0x09
#define SUB 0x0a
#define MUL 0x0b
#define DIV 0x0c
#define MOD 0x0d
#define EQ 0x0e
#define NE 0x0f
#define LT 0x10
#define GT 0x11
#define LE 0x12
#define GE 0x13
#define NOT 0x14
#define AND 0x15
#define OR 0x16
#define INPUT 0x17
#define OUTPUT 0x18
#define CLOCK 0x2a

#define NEXT_INSTRUCTION goto *label_tab[*pc]

int32_t a32, b32;       // for comparison, arithmetic and binary operations
uint8_t *pc;            // pointer for Program Counter
uint8_t io_ch;          // used for io operations
// uint8_t opcode;
uint8_t code[65537] = {0};
vector<int32_t> stack;



int main(int argc, char const *argv[]) {
    if (argc != 2) {
        printf("Wrong number of arguments!\n");
        exit(1);
    }
    // Read from file, name given in argv
    FILE *bytecode;
    bytecode = fopen(argv[1], "rb");
    uint16_t bytes_read = 0;
    if ((bytes_read = fread(code, 1, 65536, bytecode)) <= 0) {
        printf("Problem with reading the file...\n");
        fclose(bytecode);
        exit(1);
    }
    fclose(bytecode);
    // Initializing pointers to the labels for Idirect Threaded Interpreter
    static void *label_tab[43] = {
        &&halt_lb, &&jump_lb, &&jnz_lb, &&dup_lb,
        &&swap_lb ,&&drop_lb ,&&push4_lb, &&push2_lb, &&push1_lb,
        &&add_lb, &&sub_lb, &&mul_lb, &&div_lb, &&mod_lb,
        &&eq_lb, &&ne_lb, &&lt_lb, &&gt_lb, &&le_lb, &&ge_lb,
        &&not_lb, &&and_lb, &&or_lb, &&input_lb, &&output_lb, &&error_lb
    };
    label_tab[ 0x2a ] = &&clock_lb;
    // Last byte of code[] (size = 65536+1) is always 0, for programs with no
    // HALT at the end to finish
    // For Indirect Threaded Interpeter , execute following goto (bypass switch)
    // Below exists code for switch-based and indirect threaded Interpreter
    // The code for switch-based is commented out
    // By default it is indirect threaded(switch-break etc are not executed)
    // Easily, we can change type by commenting goto commands and uncommenting
    // while, opcode, switch-case, breaks
    // Code below is for a little-endian machine

    // Save starting time
    clock_t start_time = clock();
    // Point PC to the 1st command
    pc = &code[0];
    goto *label_tab[*pc];
    // while(true) {
        // opcode = *pc;
        // switch(opcode) {
            // HALT - TERMINATE PROGRAM
            //case HALT:
            halt_lb:
                return 0;
                //break;
            // JUMP TO ADDRESS
            //case JUMP:
            jump_lb:
                pc++;
                pc = &code[0] + *((uint16_t*)pc);
                NEXT_INSTRUCTION;
                //break;
            // JUMP IF NOT ZERO
            //case JNZ:
            jnz_lb:
                pc++;
                if(stack.back() != 0) {
                    pc = &code[0] + *((uint16_t*)pc); // jump
                }
                else pc += 2; // dont jump
                stack.pop_back();
                NEXT_INSTRUCTION;
                //break;
            // ADD DUPLICATE
            //case DUP:
            dup_lb:
                pc++;
                stack.push_back(stack[(stack.size()-1) - *pc]);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // SWAP STACK ELEMENTS
            //case SWAP:
            swap_lb:
                if(stack.size() <= *(pc++)) {
                    printf("Not enough elements for SWAP.\nTerminating...\n");
                    exit(1);
                }
                swap(stack[stack.size()-1], stack[(stack.size()-1) - *pc]);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // DROP TOP ELEMENT IN STACK
            //case DROP:
            drop_lb:
                if (stack.size() == 0) {
                    printf("No element to DROP.\nTerminating...\n");
                    exit(1);
                }
                stack.pop_back();
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // PUSH 4 BYTES INTO STACK
            //case PUSH4:
            push4_lb:
                pc++;
                stack.push_back(*(int32_t*)pc);
                pc += 4;
                NEXT_INSTRUCTION;
                //break;
            // PUSH 2 BYTES INTO STACK
            //case PUSH2:
            push2_lb:
                pc++;
                stack.push_back(*(int16_t*)pc);
                pc += 2;
                NEXT_INSTRUCTION;
                //break;
            // PUSH 1 BYTE INTO STACK
            //case PUSH1:
            push1_lb:
                pc++;
                stack.push_back(*(int8_t*)pc);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // ADD
            //case ADD:
            add_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for AND\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                stack.push_back(a32 + b32);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // SUB
            //case SUB:
            sub_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for SUB\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                stack.push_back(a32 - b32);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // MUL
            //case MUL:
            mul_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for MUL\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                stack.push_back(a32 * b32);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // DIV
            //case DIV:
            div_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for DIV\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                stack.push_back(a32 / b32);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // MOD
            //case MOD:
            mod_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for MOD\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                stack.push_back(a32 % b32);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // EQUAL
            //case EQ:
            eq_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for EQ\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                if(a32 == b32) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // NOT EQUAL
            //case NE:
            ne_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for NE\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                if(a32 != b32) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // LESS
            //case LT:
            lt_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for LT\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                if(a32 < b32) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // GREATER
            //case GT:
            gt_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for GT\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                if(a32 > b32) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // LESS OR EQUAL
            //case LE:
            le_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for LE\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                if(a32 <= b32) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // GREATER OR EQUAL
            //case GE:
            ge_lb:
                if (stack.size() < 2) {
                    printf("Not enough elements in Stack for GE\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                if(a32 >= b32) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // NOT
            //case NOT:
            not_lb:
                if (stack.size() < 1) {
                    printf("Not enough elements in Stack for NOT\n");
                    exit(1);
                }
                if(a32 == 0) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            //AND
            //case AND:
            and_lb:
                if (stack.size() < 1) {
                    printf("Not enough elements in Stack for AND\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                if(a32 && b32) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            //case OR:
            or_lb:
                if (stack.size() < 1) {
                    printf("Not enough elements in Stack for OR\n");
                    exit(1);
                }
                b32 = stack.back();
                stack.pop_back();
                a32 = stack.back();
                stack.pop_back();
                if(a32 || b32) stack.push_back(1);
                else stack.push_back(0);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // INPUT
            //case INPUT:
            input_lb:
                scanf("%c", &io_ch);
                stack.push_back((uint32_t) io_ch);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // OUTPUT
            //case OUTPUT:
            output_lb:
                if (stack.size() < 1) {
                    printf("Empty stack. No available output.\n");
                    exit(1);
                }
                io_ch = stack.back();
                stack.pop_back();
                printf("%c", io_ch);
                pc++;
                NEXT_INSTRUCTION;
                //break;
            // CLOCK
            //case CLOCK:
            clock_lb:
                printf("%0.6lf\n", (double)(clock() - start_time) / CLOCKS_PER_SEC);
                pc++;
                 NEXT_INSTRUCTION;
                //break;
            //default:
            error_lb:
                printf("ERROR WITH OPCODE...\n");
                exit(1);
        //}
    //}
    return 0;
}
