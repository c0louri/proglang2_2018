main :: IO ()
main = do
    n <- getLine
    str <- getLine
    print (f (read n) str)



f n str = aux str h x y
    where
        h = tail str
        x = replicate n 1
        y = replicate n 0
        aux _ [] (a:_) _ = a
        aux v h x y = x' `seq` aux v (tail h) x' (tail x)
            where
                x' = auxs v h x y
                auxs _ [] _ _ = []
                auxs (v:vs) (h:hs) (x1:x2:xs) (y:ys) = a `seq` b `seq` (a `mod` 20130401 : b)
                    where
                        a = if v == h then x1 + x2 + 1 else x1 + x2 - y
                        b = auxs vs hs (x2:xs) ys
