import Data.Array.IO
import Data.Array

setD arr 1 = writeArray arr (1,1) 1
setD arr i = writeArray arr (i,i) 1 >> setD arr (i-1)

f arr str i j n = if j == n
    then aux arr str i j
    else aux arr str i j >> f arr str i (j+1) n
        where
            aux arr str i' j' = if j' == n
                then auxs arr i' j' (str!i') (str!j')
                else auxs arr i' j' (str!i') (str!j') >> aux arr str (i'+1) (j'+1)
                where
                    auxs arr i'' j'' a b = do
                        x <- readArray arr (i'',j''-1)
                        y <- readArray arr (i''+1,j'')
                        z <- readArray arr (i''+1,j''-1)
                        let res = if a == b then (x + y + 1) `mod` 20130401 else (x + y - z) `mod` 20130401
                        writeArray arr (i'',j'') res


main :: IO ()
main = do
    n <- getLine
    s <- getLine
    let str = listArray (1,read n) s
    arr <- newArray ((1,1),(read n, read n)) 0 ::  IO (IOUArray (Int,Int) Int)
    setD arr (read n)
    -- populate array
    f arr str 1 2 (read n)
    a <- readArray arr (1,read n)
    print a
