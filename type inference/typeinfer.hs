import Data.Char
import System.IO
import Text.Read
import qualified Data.Map as Map
import Data.Maybe

data Type  =  Tvar Int | Tfun Type Type                        deriving Eq
data Expr  =  Evar String | Eabs String Expr | Eapp Expr Expr  deriving (Eq, Show)

always = True    -- False omits parentheses whenever possible

instance Read Expr where
  readPrec = (do Ident x <- lexP
                 return (Evar x)) <++
             (do Punc "(" <- lexP
                 Punc "\\" <- lexP
                 Ident x <- lexP
                 Symbol "." <- lexP
                 e <- readPrec
                 Punc ")" <- lexP
                 return (Eabs x e)) <++
             (do Punc "(" <- lexP
                 e1 <- readPrec
                 e2 <- readPrec
                 Punc ")" <- lexP
                 return (Eapp e1 e2))

-- Pretty printing of types

instance Show Type where
  showsPrec p (Tvar alpha) = ("@" ++) . showsPrec 0 alpha
  showsPrec p (Tfun sigma tau) =
    showParen (p > 0) (showsPrec 1 sigma . (" -> " ++) . showsPrec 0 tau)


-- Main program

-- getTandC : returns the type of the Expression and the Constraints in a Maybe Monad
--            also returns False if a variable does not exist in the Environment
getTandC :: Expr -> Maybe (Type, [(Type, Type)])
getTandC e = if p then Just (t, c) else Nothing
    where 
        (p, t, c, cnt) = aux e (Map.fromList []) 0
        -- 1st param : Environment, it is a Map, key,val = (name, type)
        -- Constraint : (Type, Type) t1 = t2
        -- c  parameter is list of Constraints 
        -- Int is used as a counter for new Tvar
        aux :: Expr -> Map.Map String Type -> Int -> (Bool, Type, [(Type, Type)], Int)
        aux (Eapp e1 e2) env cnt = (p, Tvar cnt2, c, cnt2+1)
            where 
                p = p1 && p2
                c =  c1 ++ c2 ++[(t1, Tfun t2 (Tvar cnt2))]
                [(p1, t1, c1, cnt1), (p2, t2, c2, cnt2)] =  (p', t', c', cnt') : [(aux e2 env cnt')]
                    where
                        (p', t', c', cnt') = aux e1 env cnt
        aux (Eabs a e) env cnt = (p, Tfun (Tvar cnt) t, c, cnt')
            where
                (p, t, c, cnt') = aux e env' (cnt + 1)
                    where
                        env' = Map.insert a (Tvar cnt) env
        aux (Evar a) env cnt =  
            if Map.member a env -- if a exists in Environment
                then (True, (env Map.! a), [], cnt)
                else (False, Tvar 0, [], cnt)


-- replace :
--    1st arg : the type to remove
--    2nd arg : the type to add
--    3rd arg : the type in which changes will take place
replace :: Type -> Type -> Type -> Type
replace a t (Tvar x) = if a == Tvar x
                    then t 
                    else Tvar x
replace a t (Tfun x1 x2) = if a == (Tfun x1 x2)
                        then t 
                        else Tfun (replace a t x1) (replace a t x2)
       
    
-- replace_all : apply replace to all members of the list
replace_all :: Type -> Type -> [(Type, Type)] -> [(Type, Type)]
replace_all _ _ [] = []
replace_all a t ((x1, x2) : xs) = (replace a t x1, replace a t x2) : replace_all a t xs 


appear :: Type -> Type -> Bool
appear a (Tvar t) = a == Tvar t
appear a (Tfun t1 t2) = if a == (Tfun t1 t2) then True else (appear a t1) || (appear a t2)


unify :: [(Type, Type)] -> ([(Type, Type)], Bool)
unify c = aux c []
    where 
        aux :: [(Type, Type)] -> [(Type, Type)] -> ([(Type, Type)], Bool)
        aux [] s = (reverse s, True)
        aux ((Tvar a, t2) : cs) s =
            if (Tvar a) == t2 then aux cs s else
                if not (appear (Tvar a) t2)
                    then aux cs' s'
                    else ([], False)
                        where
                            cs' = replace_all (Tvar a) t2 cs
                            s' = (Tvar a, t2) : s
        aux ((t1, Tvar a) : cs) s =
            if (Tvar a) == t1 then aux cs s else
                if not (appear (Tvar a) t1)
                    then aux cs' s'
                    else ([], False)
                        where
                            cs' = replace_all (Tvar a) t1 cs
                            s'  = (Tvar a, t1) : s
        aux ((Tfun t11 t12, Tfun t21 t22) : cs) s =
            if (Tfun t11 t12) == (Tfun t21 t22) then aux cs s
            else aux cs' s 
                where 
                    cs' = cs ++ [(t11, t21), (t12, t22)]


apply_subs :: Type -> [(Type, Type)] -> Type
apply_subs t [] = t
apply_subs t ((a, b) : s) = apply_subs t' s 
    where
        t' = replace a b t

-- fix :: right sequence of numbers in Tvar
-- use a Data.Map for keeping changes and an int for the next available
fix :: Type -> Type
fix t = t'
        where
            (t', m, cnt) = aux t (Map.fromList []) 0
            aux :: Type -> Map.Map Int Int -> Int -> (Type, Map.Map Int Int, Int) 
            -- if Tvar check if a exists in Map, if yes -> change the number
            -- to the one at the value field, if no -> add a new pair (a, cnt)
            -- if Tfun parse firstly the left Type
            aux (Tvar a) m cnt =
                if Map.member a m
                then (Tvar (m Map.! a), m, cnt)
                else (Tvar cnt, m', cnt + 1)
                    where m' = Map.insert a cnt m

            aux (Tfun t1 t2) m cnt = (t, m'', cnt'')
                where
                    t = Tfun t1' t2'
                    [(t1', m', cnt'), (t2', m'', cnt'')] = (t1', m', cnt') : [aux t2 m' cnt']
                        where
                            (t1', m', cnt') = aux t1 m cnt


infer :: Expr -> (Type, Bool)
infer e = if p then ((apply_subs t' s), True) else (Tvar 0, False)
    where
        ((s, p), t') = (unify c, t) 
            where
                (t, c) = fromJust (getTandC e)


inferExpr = do s <- getLine
               let e = read s :: Expr
               let (t, p) = infer e
               if p then putStrLn (show (fix t)) else putStrLn "type error"
             
count n m  =  sequence $ take n $ repeat m

main     =  do  n <- readLn
                count n inferExpr
                                   